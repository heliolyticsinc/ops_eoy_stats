# sum up MW scanned in 2016 from historical data

# altered the .csv file by hand... site size column into numbers (with 7 decimal places), and manually deleted various rows with 'X' in column H.

import pandas as pd # used
import numpy as np
import glob
import os
import seaborn as sns
import matplotlib.pyplot as plt
from datetime import datetime, timedelta #used
from lxml import etree
from shapely.geometry import Polygon
from shapely.wkb import dumps, loads


sites = pd.read_csv('All Sites Ever Scanned & Scan Info - 2016.csv')

pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 8)

data = {'SID': sites['SID'],
        'site_name': sites['Site Name'],
        'state': sites.State,
        'date': sites['Scan Date'],
        'client': sites.Client,
        'size_mw': sites['Site Size (MWdc)'],
        }

df = pd.DataFrame(data)

df.size_mw.sum()
#1235.22  == answer! (total mw scanned in 2016)
